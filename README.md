Generate ASCII Middle Fingers anytime you want!

requires you have [aalib](http://aa-project.sourceforge.net/aalib/) intalled on your computer

### Install


```python
pip install -r requirements.txt
```

#### install with virtualenv

```python
virtualenv ./env
pip install -r requirements.txt
```

### Generate

```python
python generate.py
```


```text
QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQT??"?QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQW? 4QQQQQQQP?! .a/ _QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQP'.a 3WW??'  saWQ@? _QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQQQQQQQQQQQQQQQQQQQQ?'_wWW(   aaamQWWD!  _QQWWWWWD??????QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQQQQQQQQQQQQP\aa     wQQQQWmQQWWWQQQQQaaaaaaaaaaaaaa  ayQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQQQQT4QQQQQf_QWWQQQQQQQQQQQQQQQQQQQQQWWWWWWWWWWWB?^_wWQWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQQQQw  -!"`aQQQQQQQQQQQQQQQQQQQW???!'_aamWQWQQW6aa "??QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQQQQQ/)QQQQQQQQQQQQQQQQQQQQWW^  _aamWQQWQQQQQQQQWWWQwaa "?9WQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQQD?"ayQQQQQQQQQQQQQQQQQQQQQgjQQQQQWP'.      ?4WQQQQWWWWD' -WWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQQQg,)?WQWQQP?WQQQQQQQQQQQQQQQQQQQQwaaaayQQQQWWQQWQP?!  <ajQWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQQQWQWwc  --   4QQQQQWW?9WQWQQQQQQQWWWWQQWWQWQ?4QWWwa  .?QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQQQQQQWWm, _aaayWQQQQW/   -?4QQQQQQQQQQQQQQQQQQwaa "??   jWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQQQQQW???  QQWWWQQQQQQW, ?   )$QQQQQQQQQQQQQQQQWWWWga   -??9WQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQQQf "????4QWQQQQQQQQWQQgwQQQa ?WQQQQQQQQQQQQQQQWQa    QQQQQWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQQ? .aaaaa.-$QQQQQWQQQQa ??WWQW,-4QQWQQQQQQQQQQQQW' <aaaQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQ[ <P     $/ 4Q6. aWQQWWWWwaa,. .   ?4QQQQQQQQWQP`aQQWWWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQ[ W(     j6  QQ ]QQQQQQQQWWWQQQQga,  ??WWQQQWD\wWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQ( WmaaaaaWW  QW.)WQQQQQQQQQQQQQQQQQaaaaa "??'sWWWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQ[ ]QWWWWQQf  WQQ -$QQQQQQQQQQQQQQ@"-waaa/  _yQWWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQL )WWQQQQQ[  QQQma ?4QQQQQQQQWP?5ayQWWW?  wQQQQQQQQQQQQQQQQQQWQQQWQQQWQQQWQQQWQQQWQQQWQQQWQ
QQQQQQQQQ?  $WQQQQQ' "?QWQWQw/ ??QWWQWQQQP?$QWW' wQWWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQ@  _a  WQQQQE .a,  ?QQWQQWaa, -"""~.  QP=wQWWQQQQQQQQQQQQWQQQQQWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QP'      QQ6 4WQQQk jQQk  "????!"??T$@TT???'ajQWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
Q  jQQ/  QQQ,jWQQWgaWQQk_y,     aaa_, _saaQQQQQQQQQQWQQQQQQWQQQQQQWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
Qa  9WQ  QQQQQWQQQQWQQQQQW( ??'.QWWQQQQQWQWQQQQQQQWD???? ,aaaaaaaaa_????$QWQWQQQWQQQWQQQWQQQWQQQWQQQ
QQQ/ "Qc jQQQQQQQQQQQQQWQP _a-.jWQQQQQQQQQQQWW??!_aamQQWQQQWWWWWWWWQQQWga/"?4QQQQQQQQQQQQQQQQQQQQQQQ
QQQQw  4QQQQQQQQQQQQQQQWD _ga/ QQQQQQQQQD?!_awmQQQQQQQQQQQQQQQQQQQQQQQQWWWWga "4QQQQQQQQQQQQQQQQQQQQ
QQQQW6  "QQQQQQQQQQQQWQ?  ??? jQQQWW??^aamWQQWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQWQa/-?QQQQQQQQQQQQQQQQQQ
QQQQQf    ?QWQQQQQQWQT`  "??"<QQP?`saWQQWWQQQQQQQQQQQQQQWQQQQQQQQQQQQQQQQQQQQQWWw/-QWWQQQQQQWQQQQQQQ
QQQQQL   .   ?????"   _wmwa/ " _aBQWWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQWQQQW????$WQWa $QQQQWQ????4QQWQ
QQQQQQ _QQmaac    .s,.!???? awQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQWF' ??<aaaa;?QW6 4QQW!.aaaa7????
QQQQQQ ]QQQQWWWQWQQQQQ6a  _QWWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQaaac)mWWQajWQ,yQWsjQQWP <aaaw
QQQQQQ  ?WW "$WWQQV??'.<aa-4QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQWQQQWWWQWWWQQQWQWQQQQQQWQQQQQQQWWQQ
QQQQQQ  aa..    aa amQQQWadg-4QQQQQQQQQQWQQQQQQQQQQQQQQQQQQQQWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQ  QWQfjQQp?!wWQQQQQWQQQL ?QQQQQQQQQQQQQQWQQQWQQQQQWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQ _?4QL-P????WWwwwaa  _gWQ6 "9WWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQ _y6c-` aaaaaaaaaaaa,_a a, aa:?QQWQQQQQQQQQQQQQQQQQQQQQWQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQ )4QW  ]QWWWWWWWWWWgamyQamQQWW6 QWQQQQQWQQQQQQQQQWQQQQQQQQQQQQWQQQQQQQQQQQWQQQQQQQQQQQQQQQQQQQ
QQQQQQ .(saa -??TYT????????????????? _QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ
QQQQQQQQQgaaaa> ???TVTVTVTVTT????aajQQQQQQQQQQQQQQQQWQQQQQQQQQQQQQQQQQQQQWQQQWQQQQQQQQQQQQQQQQQQQQQQ

```