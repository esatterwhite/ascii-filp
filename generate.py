#!/usr/bin/env python
import aalib
import io
import os
from PIL import Image
from random import choice


path    = os.path
DIR     = './fingers'
fingers = os.listdir( DIR )

uri = path.abspath( path.join( DIR, choice( fingers ) ) )
screen = aalib.AsciiScreen(width=100,height=40)
image = Image.open(uri).convert('L').resize(screen.virtual_size)

screen.put_image((0,0), image);
print( screen.render() )
